<?php
 
class MG_CustomerGroupOnRegistration_Model_Observer {

    /**
     * Confirm user registration success
     * @param Varien_Event_Observer $observer
     */
    public function customerRegisterSuccess(Varien_Event_Observer $observer) {
        /* @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getEvent()->getCustomer();
        $email = $customer->getEmail();
        //TODO: better if i reload customer by email
        $group = $this->tryGetCustomerGroup($email);
        if ($group) {
            try {
                $customer->setData('group_id', $group->getCustomerGroupId());
                $customer->save();
            } catch ( Exception $e ) {
                //Mage::log()
                //TODO: log
            }
        }
    }

    private function getEmailDomain($email){
        return explode('@',$email)[1];
    }

    private function tryGetCustomerGroup($email){
        $domain = $this->getEmailDomain($email);
        $groups = Mage::getModel('customer/group')->getCollection();
        foreach($groups as $group) {
            $code = $group->getCustomerGroupCode();
            if (substr($code, 0, strlen($domain)) === $domain){
                return $group;
            }
        }
        return false;
    }

}
